package com.maddyxmm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Tomasz", "Ryba", "Czarne", "84011563273", "Ryba"));
        people.add(new Person("Krzysztof", "Okoń", "Zielone", "78082544785", "Okoń"));
        people.add(new Person("Robert", "Lewandowski", "Brazowe", "54012417284", "Lewandowski"));
        people.add(new Person("Konrad", "Zajac", "Piwne", "49120948595", "Zając"));
        people.add(new Person("Jan", "Zięba", "Niebieskie", "82091367413", "Zięba"));
        people.add(new Person("Adrianna", "Armata", "Niebieskie", "95050259672", "Nowak"));
        people.add(new Person("Zuzanna", "Dekiel", "Szare", "15052259672", "Lis"));
        System.out.println(people.toString());
        Collections.sort(people);
        System.out.println(people.toString());
    }

//    public static void bubbleSort(List<Person> people) {
//        for (int i = 0; i < people.size(); i++) {
//            for (int j = 0; j < people.size() - 1; j++) {
//                if (people.get(j).getSurname().compareTo(people.get(j + 1).getSurname()) > 0) {
//                    sort(people, j, j + 1);
//                }
//            }
//        }
//    }
//
//    public static void selectionSort(List<Person> people) {
//        for (int j = 0; j < people.size(); j++) {
//            int temp = j;
//            for (int i = j; i < people.size(); i++) {
//                if (people.get(i).getSurname().compareTo(people.get(temp).getSurname()) < 0) {
//                    temp = i;
//                }
//            }
//            sort(people, j, temp);
//        }
//    }
//
//    public static void insertionSort(List<Person> people) {
//        for (int i = 1; i < people.size(); i++) {
//            for (int j = i; j > 0; j--) {
//                if (people.get(j - 1).getSurname().compareTo(people.get(j).getSurname()) > 0) {
//                    sort(people, j, j - 1);
//                }
//            }
//        }
//    }
//
//    public static void sort(List<Person> item, int a, int b) {
//        Person temp = item.get(a);
//        item.set(a, item.get(b));
//        item.set(b, temp);
//    }
}