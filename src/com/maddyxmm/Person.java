package com.maddyxmm;

public class Person implements Comparable<Person> {
    private final String firstName;
    private final String surname;
    private final String colorEyes;
    private final String peselNumber;
    private final String familyName;

    public Person(String firstName, String surname, String colorEyes, String peselNumber, String familyName) {
        this.firstName = firstName;
        this.surname = surname;
        this.colorEyes = colorEyes;
        this.peselNumber = peselNumber;
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getColorEyes() {
        return colorEyes;
    }

    public String getPeselNumber() {
        return peselNumber;
    }

    public String getFamilyName() {
        return familyName;
    }

    @Override
    public String toString() {
        return "Person = " +
                "firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", colorEyes='" + colorEyes + '\'' +
                ", peselNumber='" + peselNumber + '\'' +
                ", familyName='" + familyName + '\'';
    }

    @Override
    public int compareTo(Person object) {
        if (surname.compareTo(object.surname) == 0) {
            if (firstName.compareTo(object.firstName) == 0) {
                if (colorEyes.compareTo(object.colorEyes) == 0) {
                    if (peselNumber.compareTo(object.peselNumber) == 0) {
                        return familyName.compareTo(object.familyName);
                    }
                    return peselNumber.compareTo(object.peselNumber);
                }
                return colorEyes.compareTo(object.colorEyes);
            }
            return firstName.compareTo(object.firstName);
        }
        return surname.compareTo(object.surname);
    }
}
